import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {
  MatGridListModule,
   MatCardModule,
   MatMenuModule,
   MatIconModule,
   MatButtonModule,
   MatToolbarModule,
   MAT_LABEL_GLOBAL_OPTIONS ,
   MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    MatTableModule,
    MatTooltipModule,
    MatTabsModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
MatListModule,
MatCheckboxModule,
MatPaginatorModule,
MatAutocompleteModule
  } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChartsPocComponent } from './charts-poc/charts-poc.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { NgxEchartsModule } from 'ngx-echarts';
@NgModule({
  declarations: [
    AppComponent,
    ChartsPocComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,HttpClientModule,MatSnackBarModule,MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatFormFieldModule,
     MatInputModule,
     MatExpansionModule,
     MatTableModule,
     MatTooltipModule,
     MatTabsModule,
     MatDialogModule,
     MatProgressSpinnerModule,
     MatSnackBarModule,
     MatSelectModule,
     MatDatepickerModule,
     MatNativeDateModule,
 MatListModule,
 MatCheckboxModule,
 MatPaginatorModule,
 MatAutocompleteModule,
BrowserAnimationsModule,
NgxEchartsModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
