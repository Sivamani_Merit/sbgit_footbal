import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
@Injectable({
  providedIn: 'root'
})
export class ChartPOCService {
  private baseUrl = environment.apiEndPoint;
  constructor(private http: HttpClient) { }
  
  get = url => {
    const endPoint = `${this.baseUrl}${url}`;
    return this.http.get(endPoint);
  };

  gettopTenSponsorListList = url => {
    return this.get(url);
  };

  getTwitterData = url => {
    return this.get(url);
  };

  getFacebookAccessToken = url => {
    return this.get(url);
  };


  getFacebookData = url => {
    return this.http.get(url);
  };
}
