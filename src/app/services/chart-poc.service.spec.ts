import { TestBed } from '@angular/core/testing';

import { ChartPOCService } from './chart-poc.service';

describe('ChartPOCService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChartPOCService = TestBed.get(ChartPOCService);
    expect(service).toBeTruthy();
  });
});
