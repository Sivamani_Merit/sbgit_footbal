import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartsPocComponent } from './charts-poc/charts-poc.component';
const routes: Routes = [{ path: 'ChartsPoc', component: ChartsPocComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
