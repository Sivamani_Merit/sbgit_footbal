import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartsPocComponent } from './charts-poc.component';

describe('ChartsPocComponent', () => {
  let component: ChartsPocComponent;
  let fixture: ComponentFixture<ChartsPocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartsPocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartsPocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
