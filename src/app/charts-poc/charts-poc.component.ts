import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartPOCService } from '../services/chart-poc.service';
import { Router } from '@angular/router';


import {
  MatSnackBar, MatTableDataSource, MatPaginator,
  MatSort,
} from '@angular/material';
import { EChartOption } from 'echarts';
declare var FB: any;
@Component({
  selector: 'app-charts-poc',
  templateUrl: './charts-poc.component.html',
  styleUrls: ['./charts-poc.component.css']
})
export class ChartsPocComponent implements OnInit {

  // keywordListData = new MatTableDataSource();
  topTensponsorList: any;
  sponsorList: string[] = [];
  sumOfvalue: string[] = [];
  countOfSponsors: string[] = [];
  chartOption: EChartOption;
  chartOption1: EChartOption;

  twitterData: any;
  facebookData: any;
  authorizeUrl: any;
  accessToken: any;
  instaData: any;
  //   @ViewChild(MatPaginator) paginator: MatPaginator;
  //   @ViewChild(MatSort) sort: MatSort;
  //  SponsorsDisplayedColumns = ['SponsorName','SumOfAnnualValue',  'CountOfSponsors'];
  constructor(private chartspocServices: ChartPOCService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.loadKeywords();
  }


  getInstaData() {
    let url = "https://api.instagram.com/v1/users/self/?access_token=1483268564.6c4e1f2.b62d2f97836748cb891a6e1298b22ab6";
    this.chartspocServices.getFacebookData(url).subscribe(
      response => {
        this.instaData = response;
      });
  }


  getFacebookData() {
    let url = "https://graph.facebook.com/me?fields=id,name,fan_count&access_token=EAAe007wO9KYBAAK7uEFF8ZAxirAiZAAqgV1lFss7BmEHyjufxavVGcFPGmGliyW9NrBcOdk6RZBYKSor4cDYFS23T35OH7ZBOZArqZA7OvCXoOsL7Fodl0ZBwR6xMd5d4YNRKplBRwODvp6Ovu0UnqZA6ZC6xoaPSZCUxLO5dkZCyzvJwZDZD";
    this.chartspocServices.getFacebookData(url).subscribe(
      response => {
        this.facebookData = response;

      });
  }

  getTwitterFollowersCount() {
    this.chartspocServices.getTwitterData("footballLeaguesData/getTwitterData").subscribe(
      response => {
        this.twitterData = response;

        //  console.log(this.twitterData);
      });
  }
 loadKeywords = () => {

    this.getTwitterFollowersCount();

    this.getFacebookData();
    // this.getFacebookAccessToken();
    //this.getFacebookData();
    this.getInstaData();

    this.chartspocServices.gettopTenSponsorListList("footballLeaguesData/getData").subscribe(
      response => {
        this.topTensponsorList = response["Data"];
        // this.keywordListData = new MatTableDataSource(response["Data"]);
        console.log("List");
        console.log(this.topTensponsorList);

        this.topTensponsorList.forEach(topTensponsorList => {
          this.sponsorList.push(topTensponsorList.SponsorName);
          this.sumOfvalue.push(topTensponsorList.SumOfAnnualValue);
          this.countOfSponsors.push(topTensponsorList.CountOfSponsors);


          let color = ['#3398DB', '#C03424'];

          this.chartOption = {
            color: color,

            title: {
              text: 'Top 10 Sponsors'
            },
            tooltip: {
              trigger: 'axis',
              axisPointer: {
                type: 'shadow'
              }
            },
            xAxis: {
              type: 'category',
              data: this.sponsorList

            },
            yAxis: [{
              type: 'value',
              scale: true,
              name: 'Anual Value',
              min: 0,
              axisLine: {
                lineStyle: {
                  color: color[0]
                }
              },

            },
            {
              type: 'value',
              scale: true,
              name: 'Number of Deals',
              min: 0,
              axisLine: {
                lineStyle: {
                  color: color[1]
                }
              },
            }],
            series: [{
              name: 'Total Anual Value',
              data: this.sumOfvalue,
              type: 'bar',

            }, {
              name: 'Deals',
              type: 'line',
              yAxisIndex: 1,
              data: this.countOfSponsors

            }]
          }
          this.chartOption1 = {
            series: [{
              type: 'treemap',
              data: [{
                name: 'Yokahoma',            // First tree
                value: 10,
                children: [{
                  name: 'Yokahoma',       // First leaf of first tree
                  value: 4
                }, {
                  name: 'Hyundai',       // Second leaf of first tree
                  value: 6
                }]
              }, {
                name: 'Chervolet',            // Second tree
                value: 20,
                children: [{
                  name: 'Chervolet',       // Son of first tree
                  value: 59.9,
                  children: [{
                    name: 'Chervolet',  // Granson of first tree
                    value: 20
                  }]
                }]
              }]
            }]
          };

        });

        // setTimeout(() => {
        //   this.keywordListData.paginator = this.paginator;
        //   this.keywordListData.sort = this.sort;
        // });


      },
      error => {
        this.alertMessage("Error occured! " + error, 4000);
      }
    );
  };

  alertMessage = (message: string, duration: number) => {
    this.snackBar.open(message, "", {
      duration: duration,
      verticalPosition: "bottom",
      horizontalPosition: "end"
    });
  };

}
