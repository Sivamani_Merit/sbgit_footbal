const dotenv = require('dotenv');
const result = dotenv.config();
if(result.error){
  throw result.error;
}
const {parsed:envs} = result;
//console.log(envs);
//console.log(process.env.JWT_KEY);

module.exports = envs;
// module.exports = {
//   apiEndPoint: process.env.API_URL,
//   masterKey: process.env.JWT_KEY,
//   port: process.env.PORT,
//   NODE_ENV:process.env.NODE_ENV
// }
