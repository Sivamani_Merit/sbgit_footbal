module.exports = {
  read_todo:
    "select a.LeagueName,a.DealsType, b.PropertyName,c.SponsorName,c.Industry,c.SubSector,c.SponsorLocation,c.SponsorRegion,c.SponsorShipType,d.DealsStartDate,d.DealsEndDate,d.DealsAnnouncedDate,d.Duration,d.AnnualvalueGBP,d.TotalvalueGBP,d.TotalvalueEuro,d.TotalvalueUSD from league a inner join property b on a.LeagueID = b.LeagueID inner join sponsor c on c.PropertyID = b.PropertyID inner join sponsordatapoints d on d.SponsorID = c.SponsorID",
  topTenSponsosrs:
    "select c.SponsorName,sum(d.AnnualvalueGBP) as SumOfAnnualValue, Count(b.PropertyName) as CountOfSponsors from league a inner join property b on a.LeagueID = b.LeagueID inner join sponsor c on c.PropertyID = b.PropertyID inner join sponsordatapoints d on d.SponsorID = c.SponsorID group by c.SponsorName order by SumOfAnnualValue desc limit 0,9"
};
