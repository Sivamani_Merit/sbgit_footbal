const queries = require("../queries/queries");
const LeaguesModel = require("../models/footballLeagues");
const LeaguesModelObj = new LeaguesModel();
const  TwitterClient = require('easy-twitter');


//Following code done for poc not used anywhere actually 

const simpleOAuth2Facebook = require('@jimmycode/simple-oauth2-facebook');
const request = require('request-promise');  

const facebook = simpleOAuth2Facebook.create({
  clientId: '2169146323170470',
  clientSecret: '39674d0ca42ad822001f5bbdc9a0b7c6',
  callbackURL: 'http://localhost:3000/api/footballLeaguesData/getFacebookCalledBack'
});

const oauth2 = require('simple-oauth2').create({
  client: {
    id: '2169146323170470',
    secret: '39674d0ca42ad822001f5bbdc9a0b7c6'
  },
  auth: {
    authorizeHost: 'https://facebook.com/',
    authorizePath: '/dialog/oauth',

    tokenHost: 'https://graph.facebook.com',
    tokenPath: '/oauth/access_token'
  }
});
//above code done for poc not used anywhere actually 

//_Create a new Twitter app and get the keys needed
const twitter = new TwitterClient({
                                  consumer_key: 'JN71tTsGuU8CPk60Iqnbkva87',
                                  consumer_secret: 'eaDFmYZ53pplZ9A8JYV7rT0MdxHzIBrU9O1fOVqjRVl6jdHc6I',
                                  access_token_key: '356003819-N2MlTn2RznIza51kaogtjH9EYZ07aqiVC1kIgvgL',
                                  access_token_secret: 'RAHtiXBxQQkE6ikOQ9OQ5xHOtoGQkImRU1oIJ9gbEgaSf'
                                });



exports.getFootballDataList = (req, res) => {
  const queryResult = async () => {
    let finalqueryResult = await LeaguesModelObj.readEntities(
      queries.topTenSponsosrs
    );
    if (!finalqueryResult) {
      res.status(500).json({
        message: "Error occured!"
      });
    } else {
      res.status(200).json({
        message: "Successfully fetched",
        Data: finalqueryResult
      });
    }
  };
  queryResult();

 
};

exports.getTwitterData = (req, res) => {
 // using promise
 twitter.getCounts('ArJuN_Rajappa')
 .then(data => {
  // console.log(data.user);
   // console.log(data.followersCount);
   res.send(data);
 // data.user : In this case 'iAmAlphaZz'
     // data.followersCount : Count of people following the account in parameter
     // data.friendsCount : count of people the twitter account in parameter is following
 })
 .catch(err => {
  res.send(err);
 })

}


//Following code done for poc not used anywhere actually if you call from node directly will work but won't work with angular call because of access control problem.
exports.getFacebookData = async (req, res) => {
  console.log("get facebook data called");
 const authorizationUri = oauth2.authorizationCode.authorizeURL({
    redirect_uri: 'http://localhost:3000/api/footballLeaguesData/getFacebookCalledBack',
    scope: ['email']
  });
   res.redirect(authorizationUri);
}

exports.getFacebookCalledBack = (req, res) => {
 
  var access_token;
  for (const key in req.query) {
      access_token =  req.query[key];
  }

  res.send(access_token);
  
}


//above code done for poc not used anywhere actually 

 
  