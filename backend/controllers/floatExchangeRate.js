const fetch = require('node-fetch');
exports.getFloatExchangeDataList = (req, res) => {
  fetch('http://www.floatrates.com/daily/gbp.json')
    .then(res => res.json())
    .then(json =>
      res.status(200).json({
        message: "Successfully fetched",
        data: { usd: json.usd.rate, euro: json.eur.rate },
      })
    )
    .catch(err =>
      res.status(500).json({
        message: "Error occured!"
      })
    );
};