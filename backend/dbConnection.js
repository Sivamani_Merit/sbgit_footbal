const mysql = require("promise-mysql");

const dbConfig = {
  host: "10.101.53.30",
  user: "root",
  password: "MeritGroup123",
  database: "sports",
  connectionLimit: 10
};

module.exports = async () => {
  try {
    let pool;
    let con;
    if (pool) con = pool.getConnection();
    else {
      pool = await mysql.createPool(dbConfig);
      con = pool.getConnection();
    }
    return con;
  } catch (ex) {
    throw ex;
  }
};
