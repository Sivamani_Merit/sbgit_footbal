//const {port} = require("./config/config");
const app = require("./app");
const debug = require("debug")("node-angular");
const http = require("http");
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;

const normalizePort = val => {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
};

const onError = error => {
  if (error.syscall !== "listen") {
    throw error;
  }
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
};

const onListening = () => {
  // const addr = server.address();
  // const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
  // debug("Listening on " + bind);
};

const port = normalizePort(process.env.PORT || "3000");
app.set("port", port);

//comment out the below 4 lines when enabling production server section below

const server = http.createServer(app);
server.on("error", onError);
server.on("listening", onListening);
server.listen(port);
console.log("Listening Node Port: "+port)


/* below section is for production server */
/*
if (cluster.isMaster) {
  console.log(`Master ${process.pid} is running`);

  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} else {
  // Workers can share any TCP connection
  // In this case it is an HTTP server
  const server = http.createServer(app);
  server.on("error",onError);
  server.on("listening",onListening);
  server.listen(port);

  console.log(`Worker ${process.pid} started`);
}
*/
// End of production server section
