const express = require("express");
const router = express.Router();
const floatExchangeRate = require("../controllers/floatExchangeRate");

// select TabMasterList
router.get("/getExchangeData", floatExchangeRate.getFloatExchangeDataList);

module.exports = router;
