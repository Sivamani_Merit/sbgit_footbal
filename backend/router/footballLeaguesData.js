const express = require("express");
const router = express.Router();
const footballLeagues = require("../controllers/footballLeagues");

// select TabMasterList
router.get("/getData", footballLeagues.getFootballDataList);

router.get("/getTwitterData", footballLeagues.getTwitterData);

//following code done for poc not used anywhere actually 
router.get("/getFacebookData", footballLeagues.getFacebookData);

router.get("/getFacebookCalledBack", footballLeagues.getFacebookCalledBack);

//above code done for poc not used anywhere actually 

module.exports = router;
