const jwt = require("jsonwebtoken");
const privateKey = 'carFileAutomationOneOftheBestSoftware'; //process.env.JWT_KEY
module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token,privateKey);
    req.userData = { user: decodedToken.user, password: decodedToken.password };
    next();
  } catch (error) {
    res.status(401).json({ message: "You are not authenticated!" });
  }
};
