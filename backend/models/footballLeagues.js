const dbConnection = require("../dbConnection");

module.exports = class LeaguesModel {
  async readEntities(query) {
    let con = await dbConnection();
    try {
      let todo = await con.query(query);
      todo = JSON.parse(JSON.stringify(todo));
      //  console.log(todo);
      return todo;
    } catch (ex) {
      console.log(ex);
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }
  async update_Exchange_Rate(euroRate,usdRate) {
    let con = await dbConnection();
    try {
      let todo = await con.query("CALL SPG_Update_Float_Rates(?,?);", [euroRate, usdRate])
      todo = JSON.parse(JSON.stringify(todo));
      console.log(todo);
      return todo;
    } catch (ex) {
      console.log(ex);
      throw ex;
    } finally {
      await con.release();
      await con.destroy();
    }
  }
};
